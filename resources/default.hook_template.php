#!/usr/bin/php
<?php
/**
 * This hook is managed by Drush to facilitate Drupal development in Git.
 */
$event = basename(__FILE__);
$command = "drush githook-event $event --live";
passthru($command, $status);
exit((int) ($status == 1));
